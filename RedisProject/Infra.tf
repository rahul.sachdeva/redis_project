provider "aws" {
  region = "us-east-1"
}

#  create VPC
resource "aws_vpc" "redis" {
  cidr_block       = "10.0.0.0/16"
  instance_tenancy = "default"
  tags = {
    Name = "redis-vpc"
  }
}

# create public subnet
resource "aws_subnet" "public_redis_subnet" {
  vpc_id                  = aws_vpc.redis.id 
  cidr_block              = "10.0.1.0/24"
  map_public_ip_on_launch = true
    tags = {
        Name = "public_redis_subnet"
}
}

# create private subnet
resource "aws_subnet" "private_redis_subnet" {
  vpc_id                  = aws_vpc.redis.id 
  cidr_block              = "10.0.2.0/24"
    tags = {
        Name = "private_redis_subnet"
}
}
resource "aws_eip" "redis_ip" {
#  instance = aws_instance.web.id
   vpc      = true
}

# create NAT Gateway
resource "aws_nat_gateway" "redis_ngw" {
  allocation_id = aws_eip.redis_ip.id
#  vpc_id        = aws_vpc.redis.id
  subnet_id     = aws_subnet.public_redis_subnet.id
  tags = {
    Name = "redis_ngw"
  }
}
#  Internet Gateway
resource "aws_internet_gateway" "redis_igw" {
 vpc_id = aws_vpc.redis.id
 tags = {
        Name = "redis_igw"
}
}
# Create the Security Group
resource "aws_security_group" "redis_SG" {
  vpc_id       = aws_vpc.redis.id
  name         = "redis Security Group"
  description  = "redis Security Group"
  
  # allow ingress of port 22
  ingress {
    cidr_blocks = ["0.0.0.0/0"]  
    from_port   = 22
    to_port     = 22
    protocol    = "tcp"
  }
  # allow ingress of port 8080
  ingress {
    cidr_blocks = ["0.0.0.0/0"]  
    from_port   = 8080
    to_port     = 8080
    protocol    = "tcp"
  } 
  # allow ingress of port 6379
  ingress {
    cidr_blocks = ["0.0.0.0/0"]  
    from_port   = 6379
    to_port     = 6381
    protocol    = "tcp"
  }  
  
  # allow egress of all ports
  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }
tags = {
   Name = "redis_SG"
}
} # end resource

# Public Route Table
resource "aws_route_table" "redis_public_route_table" {
 vpc_id = aws_vpc.redis.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_internet_gateway.redis_igw.id
  }
 tags = {
        Name = "redis_public_route_table"
}
}

# Private Route Table
resource "aws_route_table" "redis_private_route_table" {
 vpc_id = aws_vpc.redis.id
  route {
    cidr_block = "0.0.0.0/0"
    gateway_id = aws_nat_gateway.redis_ngw.id
  }
 tags = {
        Name = "redis_private_route_table"
}
}

# Associate the Route Table with the public Subnet
resource "aws_route_table_association" "redis_public_association" {
  subnet_id      = aws_subnet.public_redis_subnet.id
  route_table_id = aws_route_table.redis_public_route_table.id
}

# Associate the Route Table with the private Subnet
resource "aws_route_table_association" "redis_private_association" {
  subnet_id      = aws_subnet.private_redis_subnet.id
  route_table_id = aws_route_table.redis_private_route_table.id
}

data "template_file" "user_data" {
  template = file("add-ssh-web-app.yaml")
}

data "template_file" "user_data1" {
  template = file("add-ssh-private-app.yaml")
}

# create jenkins AWS instance
resource "aws_instance" "jenkins" {
  ami           = "ami-0669f8a6a4d65e9f3"
  instance_type = "t2.micro"
  subnet_id     = aws_subnet.public_redis_subnet.id
  key_name      = "redis_key"
  vpc_security_group_ids = [aws_security_group.redis_SG.id]
  user_data     = data.template_file.user_data.rendered
  tags = {
    Name = "jenkins"
  }
}

# create redis_master1 AWS instance
resource "aws_instance" "redis_master1" {
  ami           = "ami-0885b1f6bd170450c"
  instance_type = "t2.micro"
  subnet_id     = aws_subnet.private_redis_subnet.id
  key_name      = "redis_key"
  vpc_security_group_ids = [aws_security_group.redis_SG.id]
  user_data     = data.template_file.user_data1.rendered  
  tags = {
    Name = "redis_master1"
  }
}

# create redis_master AWS instance
resource "aws_instance" "redis_master2" {
  ami           = "ami-0885b1f6bd170450c"
  instance_type = "t2.micro"
  subnet_id     = aws_subnet.private_redis_subnet.id
  key_name      = "redis_key"
  vpc_security_group_ids = [aws_security_group.redis_SG.id]
  user_data     = data.template_file.user_data1.rendered 
  tags = {
    Name = "redis_master2"
  }
}

# create redis_master AWS instance
resource "aws_instance" "redis_master3" {
  ami           = "ami-0885b1f6bd170450c"
  instance_type = "t2.micro"
  subnet_id     = aws_subnet.private_redis_subnet.id
  key_name      = "redis_key"
  vpc_security_group_ids = [aws_security_group.redis_SG.id]
  user_data     = data.template_file.user_data1.rendered 
  tags = {
    Name = "redis_master3"
  }
}

# create redis_slave1 AWS instance
resource "aws_instance" "redis_slave1" {
  ami           = "ami-0885b1f6bd170450c"
  instance_type = "t2.micro"
  subnet_id     = aws_subnet.private_redis_subnet.id
  key_name      = "redis_key"
  vpc_security_group_ids = [aws_security_group.redis_SG.id]
  user_data     = data.template_file.user_data1.rendered 
  
  tags = {
    Name = "redis_slave1"
  }
}

# create redis_slave2 AWS instance
resource "aws_instance" "redis_slave2" {
  ami           = "ami-0885b1f6bd170450c"
  instance_type = "t2.micro"
  subnet_id     = aws_subnet.private_redis_subnet.id
  key_name      = "redis_key"
  vpc_security_group_ids = [aws_security_group.redis_SG.id]
  user_data     = data.template_file.user_data1.rendered 
  
  tags = {
    Name = "redis_slave2"
  }
}

# create redis_slave3 AWS instance
resource "aws_instance" "redis_slave3" {
  ami           = "ami-0885b1f6bd170450c"
  instance_type = "t2.micro"
  subnet_id     = aws_subnet.private_redis_subnet.id
  key_name      = "redis_key"
  vpc_security_group_ids = [aws_security_group.redis_SG.id]
  user_data     = data.template_file.user_data1.rendered 
  
  tags = {
    Name = "redis_slave3"
  }
}